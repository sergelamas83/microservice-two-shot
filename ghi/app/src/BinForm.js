import React, { useState } from 'react';

const BinForm = () => {
  const [formData, setFormData] = useState({
    closet_name: '',
    bin_number: '',
    bin_size: '',
  });
  const [error, setError] = useState(null);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8100/api/bins/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        const newBin = await response.json();
        console.log('New Bin:', newBin);

       
        setFormData({
          closet_name: '',
          bin_number: '',
          bin_size: '',
        });

        setError(null);
      } else {
        const errorData = await response.json();
        setError(errorData.message || 'Error creating bin');
      }
    } catch (error) {
      console.error('Error creating bin:', error);
      setError('Error creating bin. Please try again.');
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new bin</h1>
          <form onSubmit={handleSubmit} id="create-bin-form">
            <div className="form-floating mb-3">
              <input
                value={formData.closet_name}
                onChange={handleChange}
                placeholder="Closet Name"
                required
                type="text"
                name="closet_name"
                id="closet_name"
                className="form-control"
              />
              <label htmlFor="closet_name">Closet Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.bin_number}
                onChange={handleChange}
                placeholder="Bin Number"
                required
                type="number"
                name="bin_number"
                id="bin_number"
                className="form-control"
              />
              <label htmlFor="bin_number">Bin Number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.bin_size}
                onChange={handleChange}
                placeholder="Bin Size"
                required
                type="number"
                name="bin_size"
                id="bin_size"
                className="form-control"
              />
              <label htmlFor="bin_size">Bin Size</label>
            </div>
            {error && <p style={{ color: 'red' }}>{error}</p>}
            <button type="submit" className="btn btn-primary">Create Bin</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default BinForm;
