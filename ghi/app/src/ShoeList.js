//ghi/app/src/ShoeList.js
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const ShoeList = () => {
  const [shoes, setShoes] = useState([]);
  const [deleteMessage, setDeleteMessage] = useState('');

  const fetchShoeData = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      }
    } catch (error) {
      console.error('Error fetching shoe data:', error);
    }
  };

  const handleDeleteShoe = async (shoeId) => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        setShoes((prevShoes) => prevShoes.filter((shoe) => shoe.id !== shoeId));
        setDeleteMessage('Shoe deleted successfully');
      } else {
        setDeleteMessage('Error deleting shoe. Please try again');
      }
    } catch (error) {
      console.error('Error deleting shoe. Please try again', error);
      setDeleteMessage('Error deleting shoe. Please try again');
    }
  };

  useEffect(() => {
    fetchShoeData();
  }, []);

  return (
    <div>
      <div style={{ margin: '20px', borderBottom: '1px solid #000', paddingBottom: '10px' }}>
        <Link to="/shoes/new">Add New Shoe</Link>
        {' | '}
        <Link to="/bins/new">Add New Bin</Link>
      </div>
      <h1>Shoe List</h1>

      <ul>
        {shoes.map((shoe) => (
          <li key={shoe.id}>
            <strong>{shoe.manufacturer}</strong> - {shoe.model_name} - {shoe.color}
            {' '}

            <button onClick={() => handleDeleteShoe(shoe.id)}>Delete Shoe</button>
          </li>
        ))}
      </ul>

      {deleteMessage && (
        <div style={{ color: deleteMessage.includes('Error') ? 'red' : 'green' }}>
          <p>{deleteMessage}</p>
        </div>
      )}
    </div>
  );
};

export default ShoeList;
