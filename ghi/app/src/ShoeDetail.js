//ghi/app/src/ShoeDetail.js
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

export default function ShoeDetails() {
  const { id } = useParams();
  const [shoe, setShoe] = useState({});
  const [url, setUrl] = useState('');

  const fetchData = async () => {
    const shoeDetailUrl = `http://localhost:8080/api/shoes/${id}`;
    setUrl(shoeDetailUrl);
    const response = await fetch(shoeDetailUrl);
    if (response.ok) {
      const data = await response.json();
      setShoe(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleClick = async (e) => {
    const fetchOptions = {
      method: 'DELETE',
    };
    const response = await fetch(url, fetchOptions);
  };

  if (shoe.bin === undefined) {
    return null;
  } else {
    return (
      <>
        <div key={shoe.href} className="card" style={{ width: "16rem", margin: "8px" }}>
          <img src={shoe.picture_url} className="card-img-top" alt=" " />
          <div className="card-body" style={{ borderBottom: "solid blue line" }}>
            <h5 className="card-title">{shoe.model_name}</h5>
            <h6 className="card-subtitle">{shoe.manufacturer}</h6>
            <p className="card-text">{shoe.color}</p>
            <div className="card-footer">Location: {shoe?.bin?.name}</div>
          </div>
          <Link to="/shoes" className="btn btn-outline-dark" onClick={handleClick} style={{ marginTop: "10px" }}>Delete</Link>
        </div>
      </>
    );
  }
}
