import React, { useState, useEffect } from 'react';

const ShoeInput = ({ label, type, name, value, onChange }) => (
  <div className="mb-3">
    <label className="form-label">
      {label}:
      <input
        type={type}
        className="form-control"
        name={name}
        value={value}
        onChange={(e) => onChange(e.target.value)}
      />
    </label>
  </div>
);

const ShoeForm = ({ refreshShoes }) => {
  const [manufacturer, setManufacturer] = useState('');
  const [model, setModel] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);
  const [createMessage, setCreateMessage] = useState('');

  const fetchBins = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/bins/');
      if (response.ok) {
        const binData = await response.json();
        setBins(binData.bins);
      } else {
        console.error('Error fetching bins:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching bins:', error);
    }
  };

  useEffect(() => {
    fetchBins();
  }, []);

  const handleInputChange = (value, setterFunction) => {
    setterFunction(value);
  };

  const submitForm = async () => {
    const data = {
      manufacturer,
      model,
      color,
      picture,
      bin,
    };

    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(shoesUrl, fetchConfig);
      if (response.ok) {
        refreshShoes();
        setManufacturer('');
        setModel('');
        setColor('');
        setPicture('');
        setBin('');
        setCreateMessage('Shoe successfully created!');
      } else {
        setCreateMessage('Failed to create a shoe.');
      }
    } catch (error) {
      console.error('Error:', error);
      setCreateMessage('Error creating a shoe.');
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    await submitForm();
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Shoe</h1>
          <form onSubmit={handleSubmit}>
            <ShoeInput
              label="Manufacturer"
              type="text"
              name="manufacturer"
              value={manufacturer}
              onChange={value => handleInputChange(value, setManufacturer)}
            />
            <ShoeInput
              label="Model"
              type="text"
              name="model"
              value={model}
              onChange={value => handleInputChange(value, setModel)}
            />
            <ShoeInput
              label="Color"
              type="text"
              name="color"
              value={color}
              onChange={value => handleInputChange(value, setColor)}
            />
            <ShoeInput
              label="Picture URL"
              type="text"
              name="picture"
              value={picture}
              onChange={value => handleInputChange(value, setPicture)}
            />
            <div className="mb-3">
              <label className="form-label">
                Bin:
                <select
                  className="form-control"
                  name="bin"
                  value={bin}
                  onChange={(e) => handleInputChange(e.target.value, setBin)}
                >
                  <option value="">Select a Bin</option>
                  {bins.map((binOption) => (
                    <option key={binOption.id} value={binOption.id}>
                      {binOption.closet_name} - {binOption.bin_number}/{binOption.bin_size}
                    </option>
                  ))}
                </select>
              </label>
            </div>

            <div>
              <button type="submit" className="btn btn-primary">
                Create Shoe
              </button>
            </div>
          </form>

          {createMessage && (
            <div style={{ color: 'green', marginTop: '10px' }}>
              <p>{createMessage}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ShoeForm;
