//ghi/app/src/App.js

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import ShoeDetails from './ShoeDetail';
import BinForm from './BinForm';

function App() {
  const refreshShoes = () => {

    console.log('Refreshing shoes...');
  };

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />

          <Route path="/shoes/new" element={<ShoeForm refreshShoes={refreshShoes} />} />
          <Route path="/shoes/:id" element={<ShoeDetails />} />
          <Route path="/bins/new" element={<BinForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
