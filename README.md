# Wardrobify

Team:

* Person 1 - Which microservice? Serge Lamas- Shoes
* Person 2 - Which microservice? 

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The models defined in the microservice shoe_rest for BinVO and Shoe are set as follows:
BinVO represent bins with attributeslike import_href and name.
Shoe represents shoes with attributes such as manufacturer, model_name, color, picture_url
and a ForeignKey with BinVO model that represents the connection between the Shoes and Bins
this model store the nessesary data for each Shoe and integrates with wardrobify using the BinVO model
that stores a vertual object for every bin allowing for tracking shoe storage and with out affecting the bins in wardrobe

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
