# shoes_rest/models.py
from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    id= models.AutoField(auto_created=True, primary_key=True,serialize=False,verbose_name="ID")
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"id": self.id})

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=255)
    model_name = models.CharField(max_length=255)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.model_name
