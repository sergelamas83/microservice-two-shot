# shoes_rest/api_views.py
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model",
        "color",
        "name", 
        
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.bin_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer", 
        "name",
        "model", 
        "color", 
        "picture",
        "bin",
    ]

@require_http_methods(["GET", "POST"])
def api_list_shoe(request, bin_vo_id=None):
    if request.method == "POST":
        content = json.loads(request.body)

        try:
            bin_href = content.get("bin")
            bin_queryset = BinVO.objects.filter(import_href=bin_href)
            bin = bin_queryset.get()

            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location ID"}, status=400)

        shoe = Shoes.objects.create(**content)

        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        shoes = Shoes.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeDetailEncoder,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_delete(request, id):
    if request.method == "GET":
        shoe = get_object_or_404(Shoes, id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"message": "Shoe deleted successfully"}, status=204)
        else:
            return JsonResponse({"error": "Shoe not found", "message": "Shoe not found"}, status=404)
