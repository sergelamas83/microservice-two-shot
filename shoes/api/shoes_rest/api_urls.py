# shoes_rest/api_urls.py
from django.urls import path
from .api_views import api_list_shoe, api_show_delete

urlpatterns = [
    path('shoes/', api_list_shoe, name='api_list_shoe'),
    path('shoes/<int:id>/', api_show_delete, name='api_show_delete'),
]
