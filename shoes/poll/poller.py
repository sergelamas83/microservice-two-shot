#poll/poller.py
import django
import os
import sys
import time
import json
import requests




sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()


from shoes_rest.models import BinVO

def get_bins():
    url = "http://wardrobe-api:8100/api/bins"
    response = requests.get(url)
    content = json.loads(response.content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["name"],
                "bin size": bin["bin_size"],
                "bin_number": bin["bin_number"],
            },
        )
        print("BinsSuck")

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e)
        time.sleep(5)


if __name__ == "__main__":
    poll()
